<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = [
        // 'name',
        // 'email',
        // 'telefone',
        // 'CPF',
        // 'CEP',
        // 'endereco',
        // 'numero',
        // 'bairro'
        'razao',
        'fantasia',
        'cnpj',
        'email',
        'efinanceiro',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'cidade',
        'uf',
        'complemento',
        'celular',
        'telefone',
    ];
}
