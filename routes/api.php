<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\ParceirosController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Route::resource('/clients', ClientController::class);

Route::post('/register', [AuthController::class, 'register']); 
Route::post('/login', [AuthController::class, 'login']); 
//Rotas publicas de clientes
 Route::get('/clients', [ClientController::class, 'index']);
 Route::post('/clients', [ClientController::class, 'store']); 
 Route::get('/clients/{id}', [ClientController::class, 'show']);
 Route::put('/clients/{id}', [ClientController::class, 'update']);
 Route::delete('/clients/{id}', [ClientController::class, 'destroy']);
 Route::get('/parceiros/search/{name}', [ParceirosController::class, 'search']);
 Route::post('/parceiros', [ParceirosController::class, 'store']); 
Route::put('/parceiros/{id}', [ParceirosController::class, 'update']);
Route::delete('/parceiros/{id}', [ParceirosController::class, 'destroy']);
 //Rotas publicas de parceiros
 Route::get('/parceiros', [ParceirosController::class, 'index']);
 Route::get('/parceiros/{id}', [ParceirosController::class, 'show']);
 Route::get('/parceiros/search/{name}', [ParceirosController::class, 'search']);


 //Proteção de rotas
 Route::group(['middleware' => ['auth:sanctum']], function () {
//Rotas de Clientes
    Route::post('/logout', [AuthController::class, 'logout']); 
//Rotas de Parceiros
    
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
 